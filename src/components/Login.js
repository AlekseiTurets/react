import React, { Component } from 'react';
import Moment from 'moment';
import {saveCurrentUser} from '../utils/Users';
import {saveData, loadData} from '../utils/Storage';

const demo = [
    {
        'id': 1,
        'firstName': 'John',
        'lastName': 'Aik',
        'email': 'john@gmail.com',
        'password': 'qwerty',
        'lastActivity': Moment.unix(),
        'isAdmin': true
    },
    {
        'id': 2,
        'firstName': 'Sarah',
        'lastName': 'Connor',
        'email': 'sarah@gmail.com',
        'password': 'qwerty',
        'lastActivity': Moment.unix(),
        'isAdmin': false
    },
    {
        'id': 3,
        'firstName': 'Bill',
        'lastName': 'Murray',
        'email': 'bill@gmail.com',
        'password': 'qwerty',
        'lastActivity': Moment.unix(),
        'isAdmin': false
    },
    {
        'id': 4,
        'firstName': 'Scarlett',
        'lastName': 'Johansson',
        'email': 'scarlett@gmail.com',
        'password': 'qwerty',
        'lastActivity': Moment.unix(),
        'isAdmin': false
    },
    {
        'id': 5,
        'firstName': 'Tom',
        'lastName': 'Hanks',
        'email': 'tom@gmail.com',
        'password': 'qwerty',
        'lastActivity': Moment.unix(),
        'isAdmin': false
    }
];

class Login extends Component {
    constructor(props) {
        super(props);
        Login.defaultStorage();
        this.state = {
            email: '',
            password: '',
            alertEmail: false,
            alertPassword: false,
            alertSession: props.alertSession,
            showDemo: false
        };
        this.onChangeEmail = this.onChangeEmail.bind(this);
        this.onChangePassword = this.onChangePassword.bind(this);
        this.onSubmitLogin = this.onSubmitLogin.bind(this);
        this.onClickDemo = this.onClickDemo.bind(this);
    }

    onChangeEmail(e) {
        let regExp = new RegExp('^[0-9a-z_]+@[0-9a-z_]+(\\.[a-z]{2,5})$');
        this.setState({
            email: e.target.value,
            alertEmail: !regExp.exec(e.target.value),
            alertSession: false
        });
    }

    onChangePassword(e) {
        let regExp = new RegExp('^[0-9a-z]{1,50}$');
        this.setState({
            password: e.target.value,
            alertPassword: !regExp.exec(e.target.value),
            alertSession: false
        });
    }

    onSubmitLogin(e) {
        e.preventDefault();
        let {alertEmail, alertPassword} = this.state;

        if (!alertEmail && !alertPassword) {
            let users = loadData('users');

            if (users) {
                users.forEach((user) => {
                    let {email, password} = this.state;
                    if (user.email === email && user.password === password) {
                        saveCurrentUser(user);
                        this.props.setCurrentUser({currentUser: user});
                        this.props.setWindows();
                    }
                });
            }
        }
    }

    onClickDemo() {
        this.setState({
            showDemo: !this.state.showDemo
        });
    }

    getDemo() {
        return demo.map((e, index) => {
            return (
                <li key={index}>
                    <div>
                        <span>Емейл: </span>
                        <span>{e.email}</span>
                    </div>
                    <div>
                        <span>Пароль: </span>
                        <span>{e.password}</span>
                    </div>
                </li>
            );
        });
    }

    static defaultStorage() {
        let users = loadData('users');

        if (!users) {
            saveData('users', demo);
        }
    }

    render () {
        let {email, password, alertEmail, alertPassword, alertSession, showDemo} = this.state;

        return (
            <div className="Login">
                <img src={require('../images/logoWhite.png')} alt="Logo"/>
                <form>
                    {alertSession ? (
                        <div className="alert">Сессия истекла</div>
                    ) : null}
                    {alertEmail ? (
                        <div className="alert">Введите действующий адрес эл. почты.</div>
                    ) : null}
                    <div>
                        {alertEmail ? (
                            <div className="wrapperEmail">
                                <div className="alertSmall"/>
                            </div>
                        ) : null}
                        <input
                            className="email"
                            type="email"
                            placeholder="Эл. почта"
                            value={email}
                            onChange={this.onChangeEmail}
                            max="50"
                        />
                    </div>
                    {alertPassword ? (
                        <div className="alert">Только цифры и буквы латинского алфавита.</div>
                    ) : null}
                    <div>
                        <div>
                            {alertPassword ? (
                                <div className="wrapperEmail">
                                    <div className="alertSmall"/>
                                </div>
                            ) : null}
                            <input
                                className="password"
                                type="password"
                                placeholder="Пароль"
                                value={password}
                                onChange={this.onChangePassword}
                                max="50"
                            />
                        </div>
                    </div>
                    <div>
                        <span className="submitLogin" onClick={this.onSubmitLogin}>
                            Вход
                        </span>
                    </div>
                </form>
                <div id="demoWrapper">
                    <span onClick={this.onClickDemo}>Демо</span>
                    {showDemo ? (
                        <div className="demo">
                            <ul>
                                {this.getDemo()}
                            </ul>
                        </div>
                    ) : null}
                </div>
            </div>
        )
    }
}

export default Login;