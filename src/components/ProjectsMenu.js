import React, { Component } from 'react';
import {
    MENU_LIST,
    MENU_TABLE,
    MENU_TIMELINE
} from '../constants/Constants';
import {getTimelineDateRange} from '../utils/Timeline';
import '../css/index.css';

class ProjectsMenu extends Component {
    constructor(props) {
        super(props);
        this.onClickList = this.onClickList.bind(this);
        this.onClickTable = this.onClickTable.bind(this);
        this.onClickTimeline = this.onClickTimeline.bind(this);
    }

    onClickList() {
        let {setWindows, projectIssuesList, project} = this.props;
        setWindows('project', projectIssuesList, project, {
            project: project,
            projectMenu: MENU_LIST,
            showSelect: false
        });
    }

    onClickTable() {
        let {setWindows, projectIssuesList, project} = this.props;
        setWindows('project', projectIssuesList, null, {
            project: project,
            projectMenu: MENU_TABLE,
            showSelect: false
        });
    }

    onClickTimeline() {
        let {setWindows, projectIssuesList, project} = this.props;

        setWindows('project', projectIssuesList, null, {
            project: project,
            projectMenu: MENU_TIMELINE,
            dateRange: getTimelineDateRange(projectIssuesList, project),
            showSelect: false
        });
    }

    render() {
        let {projectMenu} = this.props;
        let className = 'menuActive';

        return (
            <ul className={`ProjectsMenu margin${projectMenu !== MENU_LIST ? ' menuTable' : ''}`}>
                <li onClick={this.onClickList} className={projectMenu === MENU_LIST ? className : ''}>Список</li>
                <li onClick={this.onClickTable} className={projectMenu === MENU_TABLE ? className : ''}>Таблица</li>
                <li onClick={this.onClickTimeline} className={projectMenu === MENU_TIMELINE ? className : ''}>Временная шкала</li>
            </ul>
        );
    }
}

export default ProjectsMenu;