import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import PropTypes from 'prop-types';
import Moment from 'moment';
import App from './App';
import Task from './Task';
import Select from './Select';
import SelectProject from './SelectProject';
import {
    getNewtId,
    saveNewData,
    saveData,
    loadData
} from '../utils/Storage';
import {getUsers} from '../utils/Users';
import {filterByParam} from '../utils/Filters';
import {getSelect, hideUserDeleteButtons, getWord} from '../utils/Common';
import {getProjectIssuesList, getIssuesList} from '../utils/Issues';
import {
    ISSUE_OPTIONS_LIST,
    DATE_FORMAT,
    DATE_FORMAT_MONTH_SHORT_DAY,
    DATE_FORMAT_H_MM,
    WINDOW_ISSUE_DETAIL
} from '../constants/Constants';
import '../css/index.css';

class IssueDetail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            objProject: props.project,
            objIssue: props.issueDetail,
            usersSelectIsHidden: true,
            tasks: props.issueDetail ? this.getTasksByIssueId(props.issueDetail.id) : null,
            newTaskName: '',
            showDotsMenu: false,
            showDescription: true,
            showTasks: false
        };
        this.onDeleteUser = this.onDeleteUser.bind(this);
        this.onClickDeleteIssue = this.onClickDeleteIssue.bind(this);
        this.onClickAddUser = this.onClickAddUser.bind(this);
        this.onChangeProject = this.onChangeProject.bind(this);
        this.onChangeStatus = this.onChangeStatus.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onBlurDescription = this.onBlurDescription.bind(this);
        this.onChangeUsers = this.onChangeUsers.bind(this);
        this.onChangeTaskName = this.onChangeTaskName.bind(this);
        this.onSubmitAddTask = this.onSubmitAddTask.bind(this);
        this.onClickDots = this.onClickDots.bind(this);
        this.onClickDates = this.onClickDates.bind(this);
        this.onClickTasks = this.onClickTasks.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        hideUserDeleteButtons();

        this.setState({
            objProject: nextProps.project,
            objIssue: nextProps.issueDetail,
            tasks: nextProps.issueDetail ? this.getTasksByIssueId(nextProps.issueDetail.id) : null,
            showDotsMenu: false
        });
    }

    onClickAddUser() {
        this.setState({usersSelectIsHidden: !this.state.usersSelectIsHidden});
    }

    onChangeProject(selected) {
        this.update('projectId', parseInt(selected, 10));
        this.updateWindows(this.state.objProject, this.state.objIssue);
    }

    onChangeStatus({newSelectedIssueStatus}) {
        this.update('status', parseInt(newSelectedIssueStatus, 10));
        this.updateWindows(this.state.objProject, this.state.objIssue);
    }

    onChangeDate(property, selectedMoment) {
        if (this.validateDate(property, selectedMoment)) {
            this.update(property, selectedMoment.format(DATE_FORMAT), {updateWindow: true});
        }
    }

    onChangeDescription(e) {
        this.update('description', e.target.value, {updateLocalStorage: false});
    }

    onBlurDescription(e) {
        this.updateLocalStorage('description', e.target.value);
    }

    onChangeUsers(e) {
        this.setState({usersSelectIsHidden: true});
        let selectedUser = parseInt(e.target.value, 10);

        if (selectedUser) {
            let {objIssue, objProject} = this.state;
            let usersIssues = loadData('usersIssues');
            let currentIssueId = parseInt(objIssue.id, 10);
            let usersIssuesIsExist = usersIssues ? usersIssues.filter(function(userIssue) {
                return userIssue.userId === selectedUser && userIssue.issueId === currentIssueId;
            }) : [];

            if (usersIssuesIsExist.length === 0) {
                let newUsersIssuesId = parseInt(localStorage.getItem('lastInsertIdUsersIssues'), 10) + 1;
                let newUsersIssue = {
                    id: newUsersIssuesId,
                    userId: selectedUser,
                    issueId: currentIssueId
                };
                saveNewData('usersIssues', 'lastInsertIdUsersIssues', usersIssues, newUsersIssue, newUsersIssuesId);
                this.updateWindows(objProject, objIssue);
            }
        }
    }

    onDeleteUser(e) {
        const usersIssues = loadData('usersIssues');
        const {objProject, objIssue} = this.state;

        if (usersIssues) {
            const newUsersIssues = usersIssues.filter((el) => {
                return objIssue.id !== el.issueId || (objIssue.id === el.issueId && el.userId !== parseInt(e.target.dataset.userId, 10));
            });
            saveData('usersIssues', newUsersIssues);
            this.updateWindows(objProject, objIssue);
        }
    }

    onClickDeleteIssue() {
        const {objIssue, objProject} = this.state;
        const issues = loadData('issues');
        const usersIssues = loadData('usersIssues');

        saveData('issues', filterByParam(issues, objIssue.id, 'id', false));

        if (usersIssues && usersIssues.length > 0) {
            saveData('usersIssues', filterByParam(usersIssues, objIssue.id, 'issueId', false));
        }

        this.updateWindows(objProject, objIssue);
    }

    onChangeTaskName(e) {
        this.setState({newTaskName: e.target.value});
    }

    onSubmitAddTask(e) {
        e.preventDefault();
        let {newTaskName, objIssue} = this.state;
        let tasks = loadData('tasks');
        let newTaskId = getNewtId('lastInsertIdTasks');
        let newTask = {
            id: newTaskId,
            name: newTaskName,
            issueId: objIssue.id,
            user: null
        };
        let newTasks = saveNewData('tasks', 'lastInsertIdTasks', tasks, newTask, newTaskId);

        let filteredTasks = this.getTasksByIssueId(objIssue.id, newTasks);
        this.setState({
            tasks: filteredTasks,
            newTaskName: ''
        });
    }

    onClickDots() {
        this.setState({
            showDotsMenu: !this.state.showDotsMenu
        });
    }

    onClickDates() {
        this.setState({
            showDescription: true,
            showTasks: false
        });
    }

    onClickTasks() {
        this.setState({
            showDescription: false,
            showTasks: true
        });
    }

    update(property, value, params = {}) {
        let {updateLocalStorage = true, updateWindow = false} = params;
        let objIssue = this.state.objIssue;
        objIssue[property] = value;
        this.setState({objIssue: objIssue});

        if (updateLocalStorage) {
            this.updateLocalStorage(property, value, updateWindow);
        }
    }

    updateLocalStorage(property, value, updateWindow) {
        let issues = loadData('issues');
        let issueDetailId = this.props.issueDetail.id;
        let newIssues = issues.map(function(issue) {
            if (issueDetailId === issue.id) {
                issue[property] = value;
            }
            return issue;
        });
        saveData('issues', newIssues);

        if (updateWindow) {
            let {objProject, objIssue} = this.state;
            this.updateWindows(objProject, objIssue);
        }
    }

    updateWindows(project, issue) {
        if (App.window) {
            let windowMain = null;
            let params = {};

            if (App.window === 'projectsIssue') {
                windowMain = getProjectIssuesList(project);
                params = {project: project};
            } else if (App.window === 'issue') {
                windowMain = getIssuesList();
                params = {project: project};
            }

            this.props.setWindows(App.window, windowMain, issue, params);
        }
    }

    validateDate(property, selectedMoment) {
        let {objIssue} = this.state;

        if (property === 'dateStart') {
            let endMoment = Moment(objIssue.dateEnd);
            return selectedMoment.isSameOrBefore(endMoment);
        } else if (property === 'dateEnd') {
            let startMoment = Moment(objIssue.dateStart);
            return startMoment.isSameOrBefore(selectedMoment);
        }
        return false;
    }

    getTasksByIssueId(issueId, tasks = loadData('tasks')) {
        return tasks ? tasks.filter(function(e) {
            return e.issueId === issueId;
        }) : null;
    }

    renderTasks() {
        const {tasks, showTasks, objIssue, objProject} = this.state;

        return tasks ? (
            tasks.map((e, index) => {
                return (
                    <tr key={index} hidden={!showTasks}>
                        <td colSpan="4">
                            <Task
                                task={e}
                                project={objProject}
                                issue={objIssue}
                                setWindows={this.props.setWindows}
                            />
                        </td>
                    </tr>
                );
            })
        ) : null;
    }

    tasksCount() {
        return this.state.tasks ? this.state.tasks.length : 0;
    }

    componentWillMount() {
        this.props.checkSession();
    }

    render() {
        let windowIssueDetail = null;
        let {issueDetail} = this.props;

        if (issueDetail) {
            let {id, projectId, name, dateStart, dateEnd, status, description} = issueDetail;
            let {
                newTaskName,
                showDotsMenu,
                showDescription,
                showTasks,
                objIssue: {
                    author: {
                        user: {firstName: authorFirstName, lastName: authorLastName},
                        date: authorDate
                    }
                }
            } = this.state;
            let momentAuthorDate = Moment(authorDate);
            let formattedAuthorDate = momentAuthorDate.isSame(Moment(), 'day') ?
                momentAuthorDate.format(DATE_FORMAT_H_MM) :
                momentAuthorDate.format(DATE_FORMAT_MONTH_SHORT_DAY);
            let momentDateStart = Moment(dateStart);
            let momentDateEnd = Moment(dateEnd);
            let displayDateStart = momentDateStart.format(DATE_FORMAT_MONTH_SHORT_DAY);
            let displayDateEnd = momentDateEnd.format(DATE_FORMAT_MONTH_SHORT_DAY);
            let select = getSelect(this.onChangeUsers, this.state.usersSelectIsHidden, 'issueDetailUsersSelect');
            let params = {
                label: false,
                callback: this.onDeleteUser
            };

            let datePickerStart = <DatePicker
                className="calendarBtn"
                customInput={<span>{displayDateStart}</span>}
                selected={Moment(dateStart)}
                onChange={this.onChangeDate.bind(this, 'dateStart')}
                dropdownMode="select"
                showYearDropdown
                monthsShown={2}
                name="dateEnd"
            />;
            let datePickerEnd = <DatePicker
                className="calendarBtn"
                customInput={<span>{displayDateEnd}</span>}
                selected={Moment(dateEnd)}
                onChange={this.onChangeDate.bind(this, 'dateEnd')}
                dropdownMode="select"
                showYearDropdown
                monthsShown={2}
                name="dateEnd"
            />;
            const tasksCount = this.tasksCount();

            windowIssueDetail = (
                <div>
                    <div className="title margin">
                        <div className="itemName">{name}</div>
                        <div className="wrapperDotsDropDown">
                            <img onClick={this.onClickDots} src={require('../images/dots.png')} alt=""/>
                            <div className="dotsDropDown" hidden={!showDotsMenu}>
                                <span className="cursorPointer" onClick={this.onClickDeleteIssue}>Удалить задачу</span>
                            </div>
                        </div>
                    </div>
                    <table>
                        <tbody>
                            <tr>
                                <td>
                                    <Select
                                        window={WINDOW_ISSUE_DETAIL}
                                        callback={this.onChangeStatus}
                                        options={ISSUE_OPTIONS_LIST}
                                        status={status}
                                    />
                                </td>
                                <td>
                                    <SelectProject
                                        callback={this.onChangeProject}
                                        projectId={projectId}
                                    />
                                </td>
                                <td className="issueUsersTD">
                                    {getUsers('issueId', id, 'usersIssues', select, 'issueDetailUsers', this.onClickAddUser, params)}
                                </td>
                                <td className="author">
                                    <span>
                                        автор: {authorFirstName.toLowerCase()} {authorLastName.charAt(0).toLowerCase()}, {formattedAuthorDate}
                                    </span>
                                </td>
                            </tr>
                            <tr className="tabs">
                                <td colSpan="2" onClick={this.onClickDates}>
                                    <span><img src={require('../images/calendar.png')} alt=""/></span>
                                    <span className="wrapperDatePicker"> {datePickerStart}</span>
                                    <span> - </span>
                                    <span className="wrapperDatePicker">{datePickerEnd}</span>
                                    <span> ({Math.abs(momentDateStart.diff(momentDateEnd, 'days')) + 'д.'})</span>
                                </td>
                                <td>
                                    <span onClick={this.onClickTasks}>
                                        <span><img src={require('../images/subtasks.png')} alt=""/></span>
                                        {tasksCount === 0 ? (
                                            <span className="addSubtask">Добавить подзадачу</span>
                                        ) : (
                                            <span>
                                                <span className="subtasksText"> {tasksCount}</span>
                                                <span className="subtasksText"> {getWord(tasksCount)}</span>
                                            </span>
                                        )}
                                    </span>
                                </td>
                                <td/>
                            </tr>
                            <tr hidden={!showDescription}>
                                <td colSpan="4">
                                    <textarea
                                        className="detailDescription"
                                        value={description}
                                        onChange={this.onChangeDescription}
                                        onBlur={this.onBlurDescription}
                                        placeholder="Нажмите, чтобы добавить описание"
                                        rows="5"
                                    />
                                </td>
                            </tr>
                            {this.renderTasks()}
                            <tr className="tasksForm" hidden={!showTasks}>
                                <td colSpan="4">
                                    <form onSubmit={this.onSubmitAddTask}>
                                        <input
                                            type="text"
                                            placeholder="Введите название новой подзадачи"
                                            value={newTaskName}
                                            onChange={this.onChangeTaskName}
                                        />
                                    </form>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            );
        }

        return (
            {windowIssueDetail} ? (
                <div className="IssueDetail">
                    {windowIssueDetail}
                </div>
            ) : null
        );
    }
}

//noinspection JSUnresolvedVariable,JSUnresolvedFunction
IssueDetail.propTypes = {
    issueDetail: PropTypes.shape({
        id: PropTypes.number,
        projectId: PropTypes.oneOfType([
            PropTypes.string,
            PropTypes.number
        ]),
        name: PropTypes.string,
        dateStart: PropTypes.string,
        dateEnd: PropTypes.string,
        status: PropTypes.number,
        description: PropTypes.string,
        checkSession: PropTypes.func
    })
};

export default IssueDetail;