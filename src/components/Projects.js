import React, {Component} from 'react';
import PropTypes from 'prop-types';
import ProjectsMenu from './ProjectsMenu';
import Select from './Select';
import SelectUser from './SelectUser';
import ProjectsIssuesList from './ProjectsIssuesList';
import ProjectsTable from './ProjectsTable';
import ProjectsTimeline from './ProjectsTimeline';
import Moment from 'moment';
import {DATE_FORMAT, OPTIONS_LIST} from '../constants/Constants';
import {getHeight, focus} from '../utils/Common';
import {getProjectIssuesList, createIssue} from '../utils/Issues';
import {issuesFilter} from '../utils/Filters';
import {
    getNewtId,
    saveNewData,
    loadData
} from '../utils/Storage';
import {
    MENU_LIST,
    MENU_TABLE,
    MENU_TIMELINE,
    WINDOW_PROJECT,
    ISSUE_ALL
} from '../constants/Constants';
import '../css/index.css';

class Projects extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectIssuesListFilter: props.projectIssuesList,
            filterVisible: !!props.projectIssuesList,
            formAddIsHidden: true,
            newIssueName: '',
            height: 100,
            projectMenu: props.projectMenu || 'list',
            selectedIssueStatus: ISSUE_ALL,
            selectedUserId: 0
        };
        this.issuesFilterCallback = this.issuesFilterCallback.bind(this);
        this.onClickAddIssue = this.onClickAddIssue.bind(this);
        this.onChangeIssueName = this.onChangeIssueName.bind(this);
        this.onSubmitAddIssue = this.onSubmitAddIssue.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        let state = {
            projectIssuesListFilter: this.issuesFilterReceiveProps(nextProps),
            filterVisible: !!nextProps.projectIssuesList
        };

        if (nextProps.projectMenu) {
            state.projectMenu = nextProps.projectMenu;
        }
        this.setState(state);
    }

    issuesFilterReceiveProps(props) {
        return issuesFilter(props.projectIssuesList, Select.selected, this.state.selectedUserId);
    }

    issuesFilterCallback({newSelectedIssueStatus = null, newSelectedUserId = null}) {
        const {selectedIssueStatus, selectedUserId} = this.state;
        const filterIssueStatus = newSelectedIssueStatus !== null ? newSelectedIssueStatus : selectedIssueStatus;
        const filterUserId = newSelectedUserId !== null ? newSelectedUserId : selectedUserId;

        this.setState({
            selectedIssueStatus: filterIssueStatus,
            selectedUserId: filterUserId,
            projectIssuesListFilter: issuesFilter(getProjectIssuesList(this.props.project), filterIssueStatus, filterUserId)
        });
    }

    onClickAddIssue() {
        this.setState({formAddIsHidden: !this.state.formAddIsHidden});
        focus(this.inputAddIssue);
    }

    onChangeIssueName(e) {
        this.setState({newIssueName: e.target.value});
    }

    onSubmitAddIssue(e) {
        e.preventDefault();
        let issues = loadData('issues');
        let newIssueId = getNewtId('lastInsertIdIssues');
        let newDate = Moment().format(DATE_FORMAT);
        let {project} = this.props;
        let newIssue = createIssue(newIssueId, project ? project.id : '', this.state.newIssueName, newDate);
        saveNewData('issues', 'lastInsertIdIssues', issues, newIssue, newIssueId);

        this.setState({
            formAddIsHidden: true,
            newIssueName: ''
        });

        if (project) {
            this.props.setWindows('projectsIssue', getProjectIssuesList(project), newIssue, {project: project});
        }
    }

    updateDimensions() {
        this.setState({
            height: getHeight(this.state.projectMenu)
        });
    }

    componentWillMount() {
        this.props.checkSession();
        this.updateDimensions();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    getContent() {
        let content = null;
        let {projectIssuesListFilter, projectMenu} = this.state;
        let {setWindows, project, dateRange, checkSession} = this.props;

        switch (projectMenu) {
            case MENU_LIST: {
                content = (
                    <ProjectsIssuesList
                        projectIssuesList={projectIssuesListFilter}
                        setWindows={setWindows}
                        project={project}
                        checkSession={checkSession}
                    />
                );
                break;
            }
            case MENU_TABLE: {
                content = (
                    <ProjectsTable
                        projectIssuesList={projectIssuesListFilter}
                        project={project}
                        checkSession={checkSession}
                    />
                );
                break;
            }
            case MENU_TIMELINE: {
                content = (
                    <ProjectsTimeline
                        projectIssuesList={projectIssuesListFilter}
                        dateRange={dateRange}
                        project={project}
                        checkSession={checkSession}
                    />
                );
                break;
            }
            default: break;
        }
        return content;
    }

    render() {
        let {
            projectIssuesListFilter,
            filterVisible,
            formAddIsHidden,
            newIssueName,
            height,
            projectMenu,
            selectedUserId
        } = this.state;
        let {fullWidthClassName, project, showSelect} = this.props;
        //noinspection JSUnresolvedVariable
        return (
            <div className={'Projects ' + fullWidthClassName} style={{height: `${height}px`}}>
                <span className="title margin">{projectMenu === MENU_LIST ? 'Задачи' : project.name}</span>
                <ProjectsMenu
                    projectIssuesList={projectIssuesListFilter}
                    setWindows={this.props.setWindows}
                    project={project}
                    projectMenu={projectMenu}
                />
                {filterVisible ? (
                    <div className="selectWrapper">
                        <Select
                            window={WINDOW_PROJECT}
                            callback={this.issuesFilterCallback}
                            projectMenu={projectMenu}
                            options={OPTIONS_LIST}
                            showFilter={showSelect}
                        />
                        <SelectUser
                            callback={this.issuesFilterCallback}
                            selectedUserId={selectedUserId}
                            showFilter={showSelect}
                        />
                    </div>
                ) : null}
                {projectMenu === MENU_LIST ? (
                    <div className="wrapperAddImg margin">
                        <span onClick={this.onClickAddIssue}>
                            <img className="addImg" src={require('../images/addBlue.png')} alt=""/>
                            Новая задача
                        </span>
                    </div>
                ) : null}
                <form className="margin" onSubmit={this.onSubmitAddIssue} hidden={formAddIsHidden}>
                    <input
                        type="text"
                        placeholder="Название задачи"
                        value={newIssueName}
                        onChange={this.onChangeIssueName}
                        ref={e => this.inputAddIssue = e}
                    />
                </form>
                {this.getContent()}
            </div>
        );
    }
}

Projects.propTypes = {
    projectDetail: PropTypes.shape({
        fullWidthClassName: PropTypes.string,
        checkSession: PropTypes.func,
        projectIssuesList: PropTypes.array
    })
};

export default Projects;