import React, {Component} from 'react';
import DatePicker from 'react-datepicker';
import PropTypes from 'prop-types';
import Moment from 'moment';
import SelectProjectStatus from './SelectProjectStatus';
import {filterByParam} from '../utils/Filters';
import {DATE_FORMAT} from '../constants/Constants';
import {
    getNewtId,
    saveNewData,
    saveData,
    loadData
} from '../utils/Storage';
import {getUsers} from '../utils/Users';
import {getSelect} from '../utils/Common';
import {hideUserDeleteButtons} from '../utils/Common';
import {getProjectIssuesList} from '../utils/Issues';
import '../css/index.css';
import 'react-datepicker/dist/react-datepicker.css';

class ProjectsDetail extends Component {
    constructor(props) {
        super(props);
        this.onDeleteUser = this.onDeleteUser.bind(this);
        this.onChangeOption = this.onChangeOption.bind(this);
        this.onChangeDate = this.onChangeDate.bind(this);
        this.onChangeDescription = this.onChangeDescription.bind(this);
        this.onBlurDescription = this.onBlurDescription.bind(this);
        this.onClickAddUser = this.onClickAddUser.bind(this);
        this.onChangeUsersSelect = this.onChangeUsersSelect.bind(this);
        this.onClickDeleteProject = this.onClickDeleteProject.bind(this);
        this.onClickDots = this.onClickDots.bind(this);
        this.state = {
            objProject: this.props.projectDetail,
            usersSelectIsHidden: true,
            showDotsMenu: false
        };
    }

    componentWillReceiveProps(nextProps) {
        hideUserDeleteButtons();

        this.setState({
            objProject: nextProps.projectDetail,
            showDotsMenu: false
        });
    }

    onChangeOption(selected) {
        this.update('status', selected);
    }

    onChangeDate(property, objMoment) {
        this.update(property, objMoment.format(DATE_FORMAT));
    }

    onChangeDescription(e) {
        this.update('description', e.target.value, false);
    }

    onBlurDescription(e) {
        this.updateLocalStorage('description', e.target.value);
    }

    onClickAddUser() {
        this.setState({usersSelectIsHidden: !this.state.usersSelectIsHidden});
    }

    onChangeUsersSelect(e) {
        this.setState({usersSelectIsHidden: true});
        let selectedUser = parseInt(e.target.value, 10);

        if (selectedUser) {
            let usersProjects = loadData('usersProjects');
            let currentProjectId = parseInt(this.state.objProject.id, 10);
            let usersProjectsIsExist = usersProjects ? usersProjects.filter(function(userProject) {
                return userProject.userId === selectedUser && userProject.projectId === currentProjectId;
            }) : null;

            if (!usersProjectsIsExist || usersProjectsIsExist.length === 0) {
                let newUsersProjectsId = getNewtId('lastInsertIdUsersProjects');
                let newUsersProject = {
                    id: newUsersProjectsId,
                    userId: selectedUser,
                    projectId: currentProjectId
                };
                saveNewData('usersProjects', 'lastInsertIdUsersProjects', usersProjects, newUsersProject, newUsersProjectsId);
            }
        }
    }

    onDeleteUser(e) {
        const usersProjects = loadData('usersProjects');
        const {objProject} = this.state;

        if (usersProjects) {
            const newUsersProjects = usersProjects.filter((el) => {
                return objProject.id !== el.projectId || (objProject.id === el.projectId && el.userId !== parseInt(e.target.dataset.userId, 10));
            });
            saveData('usersProjects', newUsersProjects);
            this.props.setWindows('project', getProjectIssuesList(objProject), objProject);
        }
    }

    onClickDeleteProject() {
        let currentProjectId = this.state.objProject.id;
        let projects = loadData('projects');
        let usersProjects = loadData('usersProjects');
        let issues = loadData('issues');

        if (projects) {
            saveData('projects', filterByParam(projects, currentProjectId, 'id', false));
        }

        if (usersProjects && usersProjects.length > 0) {
            saveData('usersProjects', filterByParam(usersProjects, currentProjectId, 'projectId', false));
        }

        if (issues && issues.length > 0) {
            saveData('issues', filterByParam(issues, currentProjectId, 'projectId', false));
        }
        this.props.projectIsDeleted(true);
    }

    onClickDots() {
        this.setState({
            showDotsMenu: !this.state.showDotsMenu
        });
    }

    update(property, value, updateLocalStorage = true) {
        let objProject = this.state.objProject;
        objProject[property] = value;
        this.setState({objProject: objProject});

        if (updateLocalStorage) {
            this.updateLocalStorage(property, value);
        }
    }

    updateLocalStorage(property, value) {
        let projects = loadData('projects');
        let projectDetailId = this.props.projectDetail.id;
        let newProjects = projects.map(function(project) {
            if (projectDetailId === project.id) {
                project[property] = value;
            }
            return project;
        });
        saveData('projects', newProjects);
    }

    componentWillMount() {
        this.props.checkSession();
    }

    static setCurrentDate() {
        return Moment().format(DATE_FORMAT);
    }

    render() {
        let projectDetail;

        if (this.props.projectDetail) {
            let {id, name, dateStart, dateEnd, status, description} = this.props.projectDetail;
            let {showDotsMenu} = this.state;
            let select = getSelect(this.onChangeUsersSelect, this.state.usersSelectIsHidden, 'pdUsersSelect');
            let params = {callback: this.onDeleteUser};

            if (!dateStart) {
                dateStart = ProjectsDetail.setCurrentDate();
            }

            if (!dateEnd) {
                dateEnd = ProjectsDetail.setCurrentDate();
            }

            let datePickerStart = <DatePicker
                className="calendarBtn"
                customInput={<img src={require('../images/calendar.png')} alt=""/>}
                selected={Moment(dateStart)}
                onChange={this.onChangeDate.bind(this, 'dateStart')}
                dropdownMode="select"
                showYearDropdown
                monthsShown={2}
                name="dateEnd"
            />;
            let datePickerEnd = <DatePicker
                className="calendarBtn"
                customInput={<img src={require('../images/calendar.png')} alt=""/>}
                selected={Moment(dateEnd)}
                onChange={this.onChangeDate.bind(this, 'dateEnd')}
                dropdownMode="select"
                showYearDropdown
                monthsShown={2}
                name="dateEnd"
            />;

            projectDetail = (
                <div>
                    <div className="title margin">
                        <div className="itemName">{name}</div>
                        <div className="wrapperDotsDropDown">
                            <img onClick={this.onClickDots} src={require('../images/dots.png')} alt=""/>
                            <div className="dotsDropDown" hidden={!showDotsMenu}>
                                <span className="cursorPointer" onClick={this.onClickDeleteProject}>Удалить проект</span>
                            </div>
                        </div>
                    </div>
                    <div className="line"/>
                    <div className="margin detailInfo">
                        {getUsers('projectId', id, 'usersProjects', select, 'pdlUsers', this.onClickAddUser, params)}
                        <div className="list">
                            <span>Дата начала проекта</span>
                            <span className="delimiter"> |</span>
                            <span> {dateStart.replace(/-/g, '/')}</span>
                            <span> {datePickerStart}</span>
                        </div>
                        <div className="list">
                            <span>Дата завершения проекта</span>
                            <span className="delimiter"> |</span>
                            <span> {dateEnd.replace(/-/g, '/')}</span>
                            <span> {datePickerEnd}</span>
                        </div>
                        <div>
                            <SelectProjectStatus
                                callback={this.onChangeOption}
                                status={status}
                            />
                        </div>
                    </div>
                    <div className="line"/>
                    <textarea
                        className="margin detailDescription"
                        value={description}
                        placeholder="Нажмите, чтобы добавить описание"
                        onChange={this.onChangeDescription}
                        onBlur={this.onBlurDescription}
                        rows="5"
                    />
                    <div className="line"/>
                </div>
            );
        }

        return (
            <div className="ProjectsDetail">
                <div className="wrapperYellowLine">
                    <div className="yellowLine"/>
                </div>
                {projectDetail}
            </div>
        );
    }
}

//noinspection JSUnresolvedVariable,JSUnresolvedFunction
ProjectsDetail.propTypes = {
    projectDetail: PropTypes.shape({
        id: PropTypes.number,
        name: PropTypes.string,
        dateStart: PropTypes.string,
        dateEnd: PropTypes.string,
        status: PropTypes.number,
        description: PropTypes.string,
        checkSession: PropTypes.func
    })
};

export default ProjectsDetail;