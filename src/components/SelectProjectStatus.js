import React, {Component} from 'react';
import {PROJECT_SUSPENDED, PROJECT_OPTIONS_LIST} from '../constants/Constants';
import {getIconArrow} from '../utils/Common';

class SelectProjectStatus extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSelect: false,
            selected: PROJECT_SUSPENDED
        };
        this.onClickSelect = this.onClickSelect.bind(this);
        this.onSelectStatus = this.onSelectStatus.bind(this);
    }

    getProjectsStatusList() {
        return Object.keys(PROJECT_OPTIONS_LIST).map((key) => {
            const e = PROJECT_OPTIONS_LIST[key];
            return (
                <div data-id={e.value} key={key} onClick={this.onSelectStatus} className="selectProjectStatus">{e.label}</div>
            );
        });
    }

    onClickSelect() {
        this.setState({showSelect: !this.state.showSelect});
    }

    onSelectStatus(e) {
        const selected = parseInt(e.target.dataset.id, 10);
        const {callback} = this.props;

        callback(selected);
        this.setState({
            showSelect: false,
            selected: selected
        });
    }

    render() {
        const {showSelect, selected} = this.state;

        return (
            <div className="SelectProjectStatus">
                <span className="wrapperSelectProjectStatus">
                    <span className="selectedProjectStatus">
                        <span>Статус: </span>
                        <span className="cursorPointer" onClick={this.onClickSelect}>
                            {PROJECT_OPTIONS_LIST[selected].label}
                        </span>
                    </span>
                    {showSelect ? (
                        <div className="selectProjectsListStatus">
                            {this.getProjectsStatusList()}
                        </div>
                    ) : null}
                </span>
                {getIconArrow(showSelect)}
            </div>
        );
    }
}

export default SelectProjectStatus;