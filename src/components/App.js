import React, { Component } from 'react';
import Menu from './Menu';
import Projects from './Projects';
import ProjectsDetail from './ProjectsDetail';
import MyIssues from './MyIssues';
import IssueDetail from './IssueDetail';
import Login from './Login';
import {
    getUserFirstLetters,
    loadCurrentUser,
    saveCurrentUser,
    sessionIsEnded
} from '../utils/Users';
import {loadData} from '../utils/Storage';
import '../css/index.css';

class App extends Component {
    static window = null;

    constructor(props) {
        super(props);
        this.state = {
            windowMain: null,
            windowDetail: null,
            projectsIsUpdated: false,
            issuesIsUpdated: false,
            currentUser: loadCurrentUser(),
            alertSession: false,
            showUserDropDown: false
        };
        this.searchIssue = '';
        this.setCurrentUser = this.setCurrentUser.bind(this);
        this.projectIsDeleted = this.projectIsDeleted.bind(this);
        this.setWindows = this.setWindows.bind(this);
        this.logout = this.logout.bind(this);
        this.checkSession = this.checkSession.bind(this);
        this.onChangeSearch = this.onChangeSearch.bind(this);
        this.onClickUserDropDown = this.onClickUserDropDown.bind(this);
    }

    setWindows(window = null, windowMain = null, windowDetail = null, params = {}) {
        let fullWidthClassName = !windowDetail ? 'fullWidth' : '';

        if (window === 'project') {
            App.window = 'project';
            let state = {};

            state.windowMain = windowMain ? (
                <Projects
                    projectIssuesList={windowMain}
                    fullWidthClassName={fullWidthClassName}
                    checkSession={this.checkSession}
                    setWindows={this.setWindows}
                    project={params.project || null}
                    projectMenu={params.projectMenu || 'list'}
                    dateRange={params.dateRange || null}
                    showFilter={params.hasOwnProperty('showSelect') ? params.showSelect : false}
                />
            ) : null;

            state.windowDetail = windowDetail ? (
                <ProjectsDetail
                    projectDetail={windowDetail}
                    projectIsDeleted={this.projectIsDeleted}
                    checkSession={this.checkSession}
                    setWindows={this.setWindows}
                />
            ) : null;

            if (state) {
                this.setState(state);
            }
        } else if(window === 'issue') {
            App.window = 'issue';
            this.setState({
                windowMain: (
                    <MyIssues
                        issuesList={windowMain}
                        fullWidthClassName={fullWidthClassName}
                        setWindows={this.setWindows}
                        checkSession={this.checkSession}
                    />
                ),
                windowDetail: windowDetail ? (
                    <IssueDetail
                        issueDetail={windowDetail}
                        setWindows={this.setWindows}
                        checkSession={this.checkSession}
                    />
                ) : null
            });
        } else if (window === 'projectsIssue') {
            App.window = 'projectsIssue';
            let projectsClassName = !windowDetail ? 'fullWidth' : '';
            this.setState({
                windowMain: (
                    <Projects
                        projectIssuesList={windowMain}
                        projectsClassName={projectsClassName}
                        project={params.project}
                        showFilter={params.hasOwnProperty('showSelect') ? params.showSelect : false}
                        checkSession={this.checkSession}
                        setWindows={this.setWindows}
                    />
                ),
                windowDetail: (
                    <IssueDetail
                        project={params.project}
                        issueDetail={windowDetail}
                        setWindows={this.setWindows}
                        checkSession={this.checkSession}
                    />
                )
            });
        } else {
            App.window = null;
            this.setState({
                windowMain: null,
                windowDetail: null
            });
        }
    }

    projectIsDeleted(isDeleted) {
        if (isDeleted) {
            this.setState({
                windowMain: null,
                windowDetail: null,
                projectsIsUpdated: true
            });
        }
    }

    setCurrentUser = ({currentUser = null, alertSession = false, showUserDropDown = null} = {}) => {
        let state = {
            currentUser: currentUser,
            alertSession: alertSession
        };

        if (showUserDropDown !== null) {
            state.showUserDropDown = showUserDropDown;
        }
        this.setState(state);
    };

    logout() {
        saveCurrentUser();
        this.setCurrentUser({showUserDropDown: false});
    }

    checkSession() {
        let currentUser = loadCurrentUser();

        if ((!currentUser && this.state.currentUser) || (currentUser && sessionIsEnded(currentUser.lastActivity))) {
            saveCurrentUser();
            this.setCurrentUser({alertSession: true});
        } else {
            saveCurrentUser(currentUser);
        }
    }

    componentWillMount() {
        this.checkSession();
    }

    onChangeSearch(e) {
        this.searchIssue = e.target.value;
        let issues = loadData('issues');
        let issuesFiltered = issues ? issues.filter((el) => {
            let searchValue = el.name.toLowerCase();
            return searchValue.indexOf(this.searchIssue.toLowerCase()) !== -1;
        }) : null;
        this.setWindows('issue', issuesFiltered);
    }

    onClickUserDropDown() {
        this.setState({
            showUserDropDown: !this.state.showUserDropDown
        });
    }

    clearSearch = () => {
        this.searchIssue = '';
    };

    render() {
        let {
            windowMain,
            windowDetail,
            projectsIsUpdated,
            currentUser,
            alertSession,
            showUserDropDown
        } = this.state;
        let appClassName = !currentUser ? 'appLogin' : '';

        return (
            <div className={`App ${appClassName}`}>
                {currentUser ? (
                    <div>
                        <div className="Header">
                            <div className="search">
                                <div>
                                    <input
                                        type="text"
                                        placeholder="Поиск"
                                        onChange={this.onChangeSearch}
                                        value={this.searchIssue}
                                    />
                                    <img src={require('../images/search.png')} alt=""/>
                                </div>
                            </div>
                            <div className="currentUser">
                                {getUserFirstLetters(currentUser)}
                                <span className="cursorPointer" onClick={this.onClickUserDropDown}>
                                    <span className="firstName">
                                        {currentUser.firstName}
                                    </span>
                                    <img src={require('../images/arrow.png')} alt=""/>
                                </span>
                                <div className="currentUserDropDown" hidden={!showUserDropDown}>
                                    <div>
                                        <span>{getUserFirstLetters(currentUser)}</span>
                                        <span className="currentUserFullName">
                                            {`${currentUser.firstName} ${currentUser.lastName}`}
                                        </span>
                                    </div>
                                    <div onClick={this.logout} className="logout">
                                        <span>Выйти</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <Menu
                            setWindows={this.setWindows}
                            projectsIsUpdated={projectsIsUpdated}
                            checkSession={this.checkSession}
                            clearSearch={this.clearSearch}
                        />
                        {windowMain}
                        {windowDetail}
                    </div>
                ) : (
                    <Login
                        setCurrentUser={this.setCurrentUser}
                        alertSession={alertSession}
                        setWindows={this.setWindows}
                    />
                )}
            </div>
        );
    }
}

export default App;
