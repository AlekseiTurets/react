import React, {Component} from 'react';
import {loadData} from '../utils/Storage';
import {getFullName} from '../utils/Users';

const SELECTED_ANY_USER = {id: 0, firstName: 'Все', lastName: ''};

class SelectUser extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSelect: false,
            selected: props.selectedUserId !== 0 ? props.selectedUserId : SELECTED_ANY_USER.firstName
        };
        this.onClickSelect = this.onClickSelect.bind(this);
        this.onSelectUser = this.onSelectUser.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({showSelect: nextProps.showSelect});
    }

    getUsersList() {
        let users = loadData('users');
        users.unshift(SELECTED_ANY_USER);

        return Object.keys(users).map((key) => {
            const e = users[key];
            const fullName = getFullName(e);

            return (
                <div data-user-id={e.id} data-name={fullName} key={key} onClick={this.onSelectUser} className="selectUser">{fullName}</div>
            );
        });
    }

    onClickSelect() {
        this.setState({showSelect: !this.state.showSelect});
    }

    onSelectUser(e) {
        const selectedUserId = parseInt(e.target.dataset.userId, 10);
        const selectedName = e.target.dataset.name;
        const {callback} = this.props;

        callback({newSelectedUserId: selectedUserId});
        this.setState({
            showSelect: false,
            selected: selectedName
        });
    }

    render() {
        const {showSelect, selected} = this.state;

        return (
            <div className="SelectUser">
                <span className="wrapperSelectUser">
                    <span className="selectedUser" onClick={this.onClickSelect}>
                        {`Исполнитель: ${selected}`}
                    </span>
                    {showSelect ? (
                        <div className="selectUsersList">
                            {this.getUsersList()}
                        </div>
                    ) : null}
                </span>
            </div>
        );
    };
}

export default SelectUser;