import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import {
    DATE_FORMAT,
    DATE_FORMAT_MONTH_SHORT_DAY_YEAR,
    DATE_FORMAT_WEEK_SHORT
} from '../constants/Constants';
import {getUsersListFullNames, getUsersProject, getUsersIssue} from '../utils/Users';
import '../css/index.css';

const rowProjectStyle = {height: '22px'};

class ProjectsTimeline extends Component {
    componentWillMount() {
        this.props.checkSession();
    }

    createTable() {
        let {dateRange: {minDate, maxDate}, projectIssuesList, project} = this.props;

        if (!projectIssuesList || !minDate || !maxDate) {
            return null;
        }

        return (
            <table>
                <tbody>
                    {this.getRowHeadDate(minDate, maxDate)}
                    {this.getRowHeadWeek(minDate, maxDate)}
                    {this.getRowProjectLine(project, minDate, maxDate)}
                    {projectIssuesList.map((el, index) => {
                        return this.getRowContent(el, index, minDate, maxDate);
                    })}
                </tbody>
            </table>
        );
    }

    getRowHeadDate(minDate, maxDate) {
        let tdList = this.getTdList(minDate, maxDate, 'week', DATE_FORMAT_MONTH_SHORT_DAY_YEAR);

        return(
            <tr>
                {tdList.map((el, index) => (
                    <td key={index} colSpan="7">{el}</td>
                ))}
            </tr>
        );
    }

    getRowHeadWeek(minDate, maxDate) {
        let tdList = this.getTdList(minDate, maxDate, 'day', DATE_FORMAT_WEEK_SHORT);

        return(
            <tr>
                {tdList.map((el, index) => (
                    <td key={index}>{el}</td>
                ))}
            </tr>
        );
    }

    getRowProjectLine(project, minDate, maxDate) {
        const usersProject = getUsersProject(project);
        const tdList = this.getTdList(minDate, maxDate, 'day');
        const projectDateStart = Moment(project.dateStart);
        const projectDateEnd = Moment(project.dateEnd);
        const classNamesList = this.getClassNames('project');

        return (
            <tr style={rowProjectStyle} className="rowProjectLine">
                {tdList.map((el, index) => {
                    const params = this.getTdParams(el, project, projectDateStart, projectDateEnd, classNamesList);
                    let {isFirstElement, isMiddleElement, isLastElement, classNames} = params;
                    let isOneDay = false;

                    if (isFirstElement && isLastElement) {
                        isLastElement = false;
                        isOneDay = true;
                    }

                    return (
                        <td key={index} className={classNames}>
                            {isFirstElement ? (
                                <div className="timelineFirstElement">
                                    <img className="triangle" src={require('../images/triangleLeft.png')} alt=""/>
                                    <span className="timelineProjectInfo">
                                        <img className="imgList" src={require('../images/list.png')} alt=""/>
                                        <span>{project.name}</span>
                                        {this.getUsersProjectList(usersProject)}
                                    </span>
                                    {isOneDay ? (
                                        <img className="triangleRight" src={require('../images/triangleRight.png')} alt=""/>
                                    ) : null}
                                </div>
                            ) : null}
                            {isMiddleElement ? (
                                <div className="timelineMiddleElement"/>
                            ) : null}
                            {isLastElement ? (
                                <div className="timelineLastElement">
                                    <img className="triangle" src={require('../images/triangleRight.png')} alt=""/>
                                </div>
                            ) : null}
                        </td>
                    )
                })}
            </tr>
        )
    }

    getRowContent(issue, index, minDate, maxDate) {
        const usersIssue = getUsersIssue(issue);
        const tdList = this.getTdList(minDate, maxDate, 'day');
        const issueDateStart = Moment(issue.dateStart);
        const issueDateEnd = Moment(issue.dateEnd);
        const classNamesList = this.getClassNames('issue');

        return(
            <tr key={index}>
                {tdList.map((el, index) => {
                    const params = this.getTdParams(el, issue, issueDateStart, issueDateEnd, classNamesList);

                    return(
                        <td key={index} className={params.classNames}>
                            <div/>
                            {params.isFirstElement ? (
                                <span className="timelineContentFirst">
                                    <span>{issue.name}</span>
                                    <span>{this.getUsersIssueList(usersIssue)}</span>
                                </span>
                            ) : null}
                        </td>
                    )
                })}
            </tr>
        );
    }

    getTdList(minDateStart, maxDateEnd, step, format = null) {
        let tdList = [];
        let _minDateStart = Moment(minDateStart.format(DATE_FORMAT));
        let _maxDateEnd = Moment(maxDateEnd.format(DATE_FORMAT));

        while(_minDateStart.isSameOrBefore(_maxDateEnd)) {
            tdList.push(format ? _minDateStart.format(format) : Moment(_minDateStart.format(DATE_FORMAT)));
            _minDateStart.add(1, step);
        }
        return tdList;
    }

    getUsersProjectList(usersProject) {
        return <span>{getUsersListFullNames(usersProject, 'project')}</span>;
    }

    getUsersIssueList(usersIssue) {
        return <span>{getUsersListFullNames(usersIssue, 'issue')}</span>;
    }

    getClassNames(prefix) {
        return {
            timeline: prefix + 'Timeline',
            timelineFirst: prefix + 'TimelineFirst',
            timelineLast: prefix + 'TimelineLast'
        }
    }

    getTdParams(mapEl, object, dateStart, dateEnd, classNamesList) {
        const timeline = mapEl.isSameOrAfter(dateStart) && mapEl.isSameOrBefore(dateEnd) ? classNamesList.timeline : '';
        const day = parseInt(mapEl.day(), 10);
        const timelineWeekend = (day === 6) || (day === 0) ? 'timelineWeekend' : '';
        const momentLastDayMonth = Moment(mapEl.format(DATE_FORMAT)).endOf('month');
        const lastDayMonth = momentLastDayMonth.isSame(mapEl, 'day') ? 'lastDayMonth' : '';
        const momentDateStart = Moment(object.dateStart);
        const momentDateEnd = Moment(object.dateEnd);
        const isFirstElement = object.dateStart === mapEl.format(DATE_FORMAT);
        const isMiddleElement = momentDateStart.isBefore(mapEl) && momentDateEnd.isAfter(mapEl);
        const isLastElement = object.dateEnd === mapEl.format(DATE_FORMAT);
        const timelineFirst = isFirstElement ? classNamesList.timelineFirst : '';
        const timelineLast = isLastElement ? classNamesList.timelineLast : '';

        return {
            classNames: `${timeline} ${timelineWeekend} ${lastDayMonth} ${timelineFirst} ${timelineLast}`,
            isFirstElement: isFirstElement,
            isMiddleElement: isMiddleElement,
            isLastElement: isLastElement
        };
    }

    render() {
        return(
            <div className="ProjectsTimeline">
                {this.createTable()}
            </div>
        )
    }
}

//noinspection JSUnresolvedVariable,JSUnresolvedFunction
ProjectsTimeline.propTypes = {
    ProjectsTimeline: PropTypes.shape({
        projectIssuesList: PropTypes.object,
        dateRange: PropTypes.object,
        project: PropTypes.object,
        checkSession: PropTypes.func,
    })
};

export default ProjectsTimeline;