import React, {Component} from 'react';
import PropTypes from 'prop-types';
import App from './App';
import {getUserFirstLetters, getFullName} from '../utils/Users';
import {loadData, saveData} from '../utils/Storage';
import {filterByParam} from '../utils/Filters';
import {getIssuesList} from '../utils/Issues';
import '../css/index.css';

class Task extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showUsersList: false,
            showRemoveBtn: false,
            usersList: this.getListUsers(props.task.user),
            user: props.task.user
        };
        this.onClickUpdateUserTask = this.onClickUpdateUserTask.bind(this);
        this.onClickUser = this.onClickUser.bind(this);
        this.onMouseEnter = this.onMouseEnter.bind(this);
        this.onMouseLeave = this.onMouseLeave.bind(this);
        this.onClickRemove = this.onClickRemove.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        this.setState({
            usersList: this.getListUsers(nextProps.task.user),
            user: nextProps.task.user
        });
    }

    onClickUpdateUserTask() {
        this.setState({showUsersList: !this.state.showUsersList});
    }

    onClickUser(user, task) {
        const tasks = loadData('tasks');

        if (tasks) {
            const {issue} = this.props;
            const newTasks = tasks.map((el) => {
                if (task.id === el.id && issue.id === el.issueId) {
                    el.user = user;
                }
                return el;
            });

            saveData('tasks', newTasks);
            this.setState({
                showUsersList: false,
                user: user
            });
        }
    }

    onMouseEnter() {
        this.setState({showRemoveBtn: true});
    }

    onMouseLeave() {
        this.setState({showRemoveBtn: false});
    }

    onClickRemove() {
        const tasks = loadData('tasks');
        const newTasks = filterByParam(tasks, this.props.task.id, 'id', false);
        saveData('tasks', newTasks);

        const {setWindows, project, issue} = this.props;
        setWindows(App.window,  getIssuesList(), issue, {project: project});
    }

    getListUsers(user) {
        const {task} = this.props;
        const users = loadData('users');
        let listUsers = null;

        if (users) {
            let usersFiltered = users.filter((e) => {
                return !user || (user && user.id !== e.id);
            });

            listUsers = usersFiltered.map((e, index) => {
                return (
                    <div key={index} className="taskUsersList" onClick={this.onClickUser.bind(this, e, task)}>
                        <div className="firstLetters">{getUserFirstLetters(e)}</div>
                        <div className="info">
                            <div>{getFullName(e)}</div>
                            <div>{e.email}</div>
                        </div>
                    </div>
                );
            });
        }
        return listUsers;
    }

    render() {
        const {task: {name}} = this.props;
        const {showUsersList, showRemoveBtn, usersList, user} = this.state;

        return (
            <div className="Task" onMouseEnter={this.onMouseEnter} onMouseLeave={this.onMouseLeave}>
                <span className="userIco">
                    {user ? (
                        <span onClick={this.onClickUpdateUserTask} className="cursorPointer">
                            {getUserFirstLetters(user)}
                        </span>
                    ) : (
                        <img src={require('../images/addUser.png')} alt="" onClick={this.onClickUpdateUserTask} className="cursorPointer"/>
                    )}
                </span>
                {showUsersList ? (
                    <div className="addUserToTask">
                        <div className="taskAddUserText">Добавьте пользователя</div>
                        {usersList}
                    </div>
                ) : null}
                <div className="tasksText">{name}</div>
                {showRemoveBtn ? (
                    <button className="removeBtn" onClick={this.onClickRemove}>Удалить</button>
                ) : null}
            </div>
        );
    }
}

//noinspection JSUnresolvedVariable
Task.propTypes = {
    task: PropTypes.object
};

export default Task;