import React, {Component} from 'react';
import Moment from 'moment';
import {DATE_FORMAT_MONTH_DAY, SECOND_DAY} from '../constants/Constants';
import {getUsersListId, getListUsersFirstLetters} from '../utils/Users';

class ProjectsIssuesList extends Component {
    componentWillMount() {
        this.props.checkSession();
    }

    getElement(items) {
        const {projectIssuesList} = this.props;
        const length = parseInt(items.length - 1, 10);

        return items.map((item, index) => {
            let usersListId = getUsersListId('issueId', item.id, 'usersIssues');
            let usersList = getListUsersFirstLetters(usersListId);

            return (
                <div key={index}>
                    <div
                        className="issuesListItems"
                        key={item.id}
                        onClick={this.onClickIssue.bind(this, projectIssuesList, item)}
                    >
                        <div>
                            {usersList.length > 0 ? usersList : <span className="userIco"><img src={require('../images/user.png')} alt=""/></span>}
                            <span className="issueName">{item.name}</span>
                            <span className="projectName">{item.projectName}</span>
                        </div>
                        <div>
                            {Moment(item.dateStart).format(DATE_FORMAT_MONTH_DAY)}
                        </div>
                    </div>
                    {length !== index ? (
                        <div className="line"/>
                    ) : null}
                </div>
            );
        });
    }

    getIssuesList(issues) {
        return Object.keys(issues).map((property, index) => {
            let e = issues[property];
            return e.items.length > 0 ? (
                <div key={index} className="wrapperIssuesListLabel">
                    <input type="checkbox" className="issuesCollapseBtn"/>
                    <div className="issuesListLabel">
                        <span className="arrow"/>
                        {e.label}
                    </div>
                    <div className="line"/>
                    <div className="issuesListElements">
                        {this.getElement(e.items)}
                    </div>
                </div>
            ) : null;
        });
    }

    onClickIssue(projectIssuesList, issue) {
        if (projectIssuesList && issue) {
            this.props.setWindows('projectsIssue', projectIssuesList, issue, {project: this.props.project})
        }
    }

    render() {
        let {projectIssuesList} = this.props;
        let issues = {
            today: {label: 'Сегодня', items: []},
            tomorrow: {label: 'Завтра', items: []},
            nextWeek: {label: 'Следующая неделя', items: []},
            later: {label: 'Позже', items: []},
            complete: {label: 'Уже прошли', items: []}
        };

        if (projectIssuesList !== null) {
            let currentMoment = Moment();
            let currentTimeStamp = currentMoment.unix();
            let timeStampToNextWeekEnd = currentMoment.add(1, 'weeks').endOf('isoWeek').unix() - currentTimeStamp;

            projectIssuesList.forEach(function(item) {
                let timestamp = Moment(item.dateStart).unix();
                let diff = timestamp - currentTimeStamp;

                if (-SECOND_DAY < diff && diff <= 0) {
                    issues.today.items.push(item);
                } else if (0 < diff && diff <= SECOND_DAY) {
                    issues.tomorrow.items.push(item);
                } else if (SECOND_DAY < diff && diff <= timeStampToNextWeekEnd) {
                    issues.nextWeek.items.push(item);
                } else if (timeStampToNextWeekEnd < diff) {
                    issues.later.items.push(item);
                } else {
                    issues.complete.items.push(item);
                }
            });
        }

        return (
            <div className="ProjectsIssuesList">
                {this.getIssuesList(issues)}
            </div>
        );
    }
}

export default ProjectsIssuesList;