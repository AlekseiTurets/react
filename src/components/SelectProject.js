import React, {Component} from 'react';
import {loadData} from '../utils/Storage';
import {getProjectNameById} from '../utils/Projects';

class SelectProject extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSelect: false,
            selected: getProjectNameById(props.projectId)
        };
        this.onClickSelect = this.onClickSelect.bind(this);
        this.onSelectProject = this.onSelectProject.bind(this);
    }

    getProjectsList() {
        let projects = loadData('projects');

        if (projects) {
            return Object.keys(projects).map((key) => {
                const e = projects[key];
                return (
                    <div data-id={e.id} data-name={e.name} key={key} onClick={this.onSelectProject} className="selectProject">{e.name}</div>
                );
            });
        }
        return null;
    }

    onClickSelect() {
        this.setState({showSelect: !this.state.showSelect});
    }

    onSelectProject(e) {
        const selectedId = parseInt(e.target.dataset.id, 10);
        const selectedName = e.target.dataset.name;
        const {callback} = this.props;

        callback(selectedId);
        this.setState({
            showSelect: false,
            selected: selectedName
        });
    }

    render() {
        const {showSelect, selected} = this.state;

        return (
            <div className="SelectProject">
                <span className="wrapperSelectProject">
                    <span className="selectedProject" onClick={this.onClickSelect}>
                        {selected}
                    </span>
                    {showSelect ? (
                        <div className="selectProjectsList">
                            {this.getProjectsList()}
                        </div>
                    ) : null}
                </span>
            </div>
        );
    }
}

export default SelectProject;