import React, {Component} from 'react';
import {getIconArrow} from '../utils/Common';
import {
    ISSUE_ALL,
    MENU_LIST,
    WINDOW_PROJECT,
    WINDOW_ISSUE_DETAIL
} from '../constants/Constants';

class Select extends Component {
    static selected = null;

    constructor(props) {
        super(props);
        this.state = {
            showSelect: false,
            selected: ISSUE_ALL
        };
        this.onClickSelect = this.onClickSelect.bind(this);
        this.onSelectStatus = this.onSelectStatus.bind(this);

        if (props.window === WINDOW_PROJECT) {
            Select.selected = ISSUE_ALL;
        }
    }

    componentWillReceiveProps(nextProps) {
        this.setState({showSelect: nextProps.showSelect});
    }

    onClickSelect() {
        this.setState({showSelect: !this.state.showSelect});
    }

    onSelectStatus(e) {
        const selected = parseInt(e.target.dataset.id, 10);
        const {callback, window} = this.props;

        callback({newSelectedIssueStatus: selected});
        this.setState({
            showSelect: false,
            selected: selected
        });

        if (window === WINDOW_PROJECT) {
            Select.selected = selected;
        }
    }

    getOptionsList() {
        const {options} = this.props;
        return Object.keys(options).map((key) => {
            const e = options[key];
            return (
                <div className="wrapperOptionsList" key={key}>
                    <div style={e.style} className="optionsListColor"/>
                    <div data-id={e.value} onClick={this.onSelectStatus} className="optionsList">{e.label}</div>
                </div>
            );
        });
    }

    render() {
        const {showSelect, selected} = this.state;
        const {projectMenu, options, window, status} = this.props;
        let statusDisplay;
        let iconArrow = null;

        switch(window) {
            case WINDOW_PROJECT: {
                statusDisplay = `Статус: ${options[selected].label}`;
                break;
            }
            case WINDOW_ISSUE_DETAIL: {
                statusDisplay = <span className="statusIssueDetail" style={options[status].style}/>;
                break;
            }
            default: statusDisplay = null;
        }

        if (window === WINDOW_ISSUE_DETAIL) {
            iconArrow = getIconArrow(showSelect);
        }

        return (
            <div className="Select">
                {projectMenu && projectMenu !== MENU_LIST ? (
                    <span className="selectView">Показать</span>
                ) : null}
                <span className="wrapperSelectStatus">
                    <span className="selectedStatus" onClick={this.onClickSelect}>
                        {statusDisplay}
                    </span>
                    {showSelect ? (
                        <div className="selectOptionsList">
                            {this.getOptionsList()}
                        </div>
                    ) : null}
                </span>
                {iconArrow}
            </div>
        );
    }
}

export default Select;