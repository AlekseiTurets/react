import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import {DATE_FORMAT, PROJECT_SUSPENDED} from '../constants/Constants';
import App from './App';
import {getProjectIssuesList, getIssuesList} from '../utils/Issues';
import {focus} from '../utils/Common';
import {
    getNewtId,
    saveNewData,
    loadData
} from '../utils/Storage';
import '../css/index.css';

class Menu extends Component {
    constructor(props) {
        super(props);
        this.state = {
            projectsList: null,
            newProjectName: '',
            formAddIsHidden: true
        };
        this.onClickProjects = this.onClickProjects.bind(this);
        this.onChangeInput = this.onChangeInput.bind(this);
        this.onClickAddProject = this.onClickAddProject.bind(this);
        this.onSubmitAddProject = this.onSubmitAddProject.bind(this);
        this.onClickMyIssues = this.onClickMyIssues.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.projectsIsUpdated === true) {
            this.setState({projectsList: this.getProjectsList(loadData('projects'))});
        }
    }

    onClickProject(project) {
        this.setState({formAddIsHidden: true});
        this.props.setWindows('project', getProjectIssuesList(project), project, {project: project});
    }

    onClickMyIssues() {
        this.setState({projectsList: null, formAddIsHidden: true});
        this.props.setWindows('issue', getIssuesList());
        this.props.clearSearch();
    }

    onClickProjects() {
        let projectsList = this.getProjectsList(loadData('projects'));

        if (projectsList) {
            this.setState({projectsList: projectsList, formAddIsHidden: true});
            this.props.setWindows();
            this.props.clearSearch();
        }
    }

    onChangeInput(e) {
        this.setState({newProjectName: e.target.value});
    }

    onClickAddProject() {
        this.setState({formAddIsHidden: !this.state.formAddIsHidden});
        focus(this.inputAddProject);
    }

    onSubmitAddProject(e) {
        e.preventDefault();
        let projects = loadData('projects');
        let newProjectId = getNewtId('lastInsertIdProjects');
        let newDate = Moment().format(DATE_FORMAT);

        let newProject = {
            id: newProjectId,
            name: this.state.newProjectName,
            dateStart: newDate,
            dateEnd: newDate,
            status: PROJECT_SUSPENDED,
            description: ''
        };
        let newProjects = saveNewData('projects', 'lastInsertIdProjects', projects, newProject, newProjectId);

        this.setState({
            projectsList: this.getProjectsList(newProjects),
            newProjectName: '',
            formAddIsHidden: true
        });

        this.updateWindows(projects);
    }

    getProjectsList(projects) {
        return projects ? projects.map((item) => {
            return (
                <li key={item.id} onClick={this.onClickProject.bind(this, item)}>
                    <span className="cursorPointer">{item.name}</span>
                </li>
            );
        }) : null;
    }

    updateWindows(projects) {
        if (
            App.window
            && App.window === 'issue'
            && (!projects || (projects && projects.length === 0))
        ) {
            this.props.setWindows(App.window, getIssuesList());
        }
    }

    componentWillMount() {
        this.props.checkSession();
    }

    render() {
        let {projectsList, formAddIsHidden, newProjectName} = this.state;

        return (
            <div className="Menu">
                <div onClick={this.onClickMyIssues}>
                    <span className="cursorPointer">Моя работа</span>
                </div>
                <div>
                    <div className="menuLeft" onClick={this.onClickProjects}>
                        <span className="cursorPointer">Проекты</span>
                    </div>
                    <div className="menuRight">
                        <img className="addImg" onClick={this.onClickAddProject} src={require('../images/add.png')} alt=""/>
                    </div>
                </div>
                <form onSubmit={this.onSubmitAddProject} hidden={formAddIsHidden}>
                    <input
                        type="text"
                        placeholder="Введите название"
                        value={newProjectName}
                        onChange={this.onChangeInput}
                        ref={e => this.inputAddProject = e}
                    />
                </form>
                <ul className="ProjectsList">
                    {projectsList}
                </ul>
            </div>
        );
    }
}

//noinspection JSUnresolvedVariable
Menu.propTypes = {
    projectsIsUpdated: PropTypes.bool,
    checkSession: PropTypes.func,
    clearSearch: PropTypes.func
};

export default Menu;