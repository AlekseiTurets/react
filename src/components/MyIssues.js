import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import {
    DATE_FORMAT_MONTH_DAY,
    DATE_FORMAT,
    SECOND_DAY,
    ISSUE_COMPLETED
} from '../constants/Constants';
import {getHeight, focus} from '../utils/Common';
import {createIssue, getIssuesList} from '../utils/Issues';
import {
    getUsersListId,
    getListUsersFirstLetters,
    getUserFirstLetters,
    loadCurrentUser
} from '../utils/Users';
import {getFirstProjectId} from '../utils/Projects';
import {
    getNewtId,
    saveNewData,
    loadData
} from '../utils/Storage';
import '../css/index.css';

class MyIssues extends Component {
    constructor(props) {
        super(props);
        this.state = {
            issuesList: props.issuesList || getIssuesList(),
            formAddIsHidden: true,
            newIssueName: '',
            height: 100,
            firstProjectId: getFirstProjectId()
        };
        this.onClickAddIssue = this.onClickAddIssue.bind(this);
        this.onChangeIssueName = this.onChangeIssueName.bind(this);
        this.onSubmitAddIssue = this.onSubmitAddIssue.bind(this);
        this.updateDimensions = this.updateDimensions.bind(this);
    }

    componentWillReceiveProps(nextProps) {
        const {firstProjectId} = this.state;

        if (!firstProjectId) {
            let newFirstProjectId = getFirstProjectId();

            if (newFirstProjectId) {
                this.setState({firstProjectId: newFirstProjectId});
            }
        }

        if (nextProps.issuesList !== null) {
            this.setState({issuesList: nextProps.issuesList || this.getWindowIssuesList(getIssuesList(nextProps.issuesList))});
        }
    }

    onClickIssue(issue) {
        this.props.setWindows('issue', null, issue);
    }

    onClickAddIssue() {
        if (this.state.firstProjectId) {
            this.setState({formAddIsHidden: !this.state.formAddIsHidden});
            focus(this.inputAddIssue);
        }
    }

    onChangeIssueName(e) {
        this.setState({newIssueName: e.target.value});
    }

    onSubmitAddIssue(e) {
        e.preventDefault();
        const {firstProjectId, newIssueName} = this.state;
        let issues = loadData('issues');
        let newIssueId = getNewtId('lastInsertIdIssues');
        let newDate = Moment().format(DATE_FORMAT);
        let newIssue = createIssue(newIssueId, firstProjectId, newIssueName, newDate);
        let newIssues = saveNewData('issues', 'lastInsertIdIssues', issues, newIssue, newIssueId);

        let usersIssues = loadData('usersIssues');
        let newUsersIssuesId = parseInt(localStorage.getItem('lastInsertIdUsersIssues'), 10) + 1;
        let newUsersIssue = {
            id: newUsersIssuesId,
            userId: loadCurrentUser().id,
            issueId: newIssueId
        };
        saveNewData('usersIssues', 'lastInsertIdUsersIssues', usersIssues, newUsersIssue, newUsersIssuesId);

        this.props.setWindows('issue', getIssuesList(newIssues), newIssue);
    }

    getWindowIssuesList(issues) {
        let self = this;
        return issues.map(function(item) {
            return (
                <div key={item.id} onClick={self.onClickIssue(item)}>
                    {item.name} ({item.projectName}-{item.id})
                </div>
            );
        });
    }

    renderIssuesList(issues) {
        return Object.keys(issues).map((property, index) => {
            let e = issues[property];
            return e.items.length > 0 ? (
                <div key={index}>
                    <div className="issuesListLabel">
                        <span>
                            <span>{e.label}</span>
                        </span>
                        <span>
                            <span>
                                {e.items.length}
                            </span>
                        </span>
                    </div>
                    {this.getElement(e.items)}
                </div>
            ) : null;
        });
    }

    getElement(items) {
        return items.map((item) => {
            let usersListId = getUsersListId('issueId', item.id, 'usersIssues');
            let usersList = getListUsersFirstLetters(usersListId);

            return (
                <div key={item.id}>
                    <div className="line"/>
                    <div className="issuesListItems" onClick={this.onClickIssue.bind(this, item)}>
                        <div>
                            {usersList.length > 0 ? (
                                <span>{usersList}</span>
                            ) : null}
                            {item.name}
                        </div>
                        <div>
                            {Moment(item.dateStart).format(DATE_FORMAT_MONTH_DAY)}
                        </div>
                    </div>
                </div>
            );
        });
    }

    updateDimensions() {
        this.setState({
            height: getHeight()
        });
    }

    componentWillMount() {
        this.props.checkSession();
        this.updateDimensions();
    }

    componentDidMount() {
        window.addEventListener("resize", this.updateDimensions);
    }

    componentWillUnmount() {
        window.removeEventListener("resize", this.updateDimensions);
    }

    getCountCompleted() {
        let filtered = this.state.issuesList.filter((e) => {
            return e.status === ISSUE_COMPLETED;
        });
        return filtered.length;
    }

    render() {
        //noinspection JSUnresolvedVariable
        let {issuesList, height, formAddIsHidden, newIssueName, firstProjectId} = this.state;
        let {fullWidthClassName} = this.props;
        let issues = {
            today: {label: 'На сегодня', items: []},
            currentWeek: {label: 'На эту неделю', items: []},
            nextWeek: {label: 'На след.неделю', items: []},
            later: {label: 'Позже', items: []}
        };

        if (issuesList && issuesList.length > 0) {
            let currentMoment = Moment().set({hour: 0, minute: 0, second: 0, millisecond: 0});
            let currentTimeStamp = currentMoment.unix();
            let timeStampToWeekEnd = currentMoment.endOf('isoWeek').unix() - currentTimeStamp;
            let timeStampToNextWeekEnd = currentMoment.add(1, 'weeks').unix() - currentTimeStamp;

            issuesList.forEach(function(item) {
                let timestamp = Moment(item.dateStart).unix();
                let diff = Math.abs(timestamp - currentTimeStamp);

                if (0 <= diff && diff < SECOND_DAY) {
                    issues.today.items.push(item);
                } else if (SECOND_DAY <= diff && diff < timeStampToWeekEnd) {
                    issues.currentWeek.items.push(item);
                } else if (timeStampToWeekEnd <= diff && diff < timeStampToNextWeekEnd) {
                    issues.nextWeek.items.push(item);
                } else if (timeStampToNextWeekEnd <= diff) {
                    issues.later.items.push(item);
                }
            });
        }

        return (
            <div className={'MyIssues ' + fullWidthClassName} style={{height: `${height}px`}}>
                <div className="margin wrapperAddBtn">
                    <span onClick={this.onClickAddIssue} className={!firstProjectId ? 'cursorDefault' : ''}>
                        <img className={`addImg ${!firstProjectId ? 'cursorDefault' : ''}`} src={require('../images/addBlue.png')} alt=""/>
                        Новая задача
                    </span>
                    {!firstProjectId ? (
                        <span className="needProject cursorDefault">Необходимо добавить проект</span>
                    ) : null}
                </div>
                <form className="margin" onSubmit={this.onSubmitAddIssue} hidden={formAddIsHidden}>
                    {getUserFirstLetters(loadCurrentUser())}
                    <input
                        type="text"
                        placeholder="Название задачи"
                        value={newIssueName}
                        onChange={this.onChangeIssueName}
                        ref={e => this.inputAddIssue = e}
                    />
                </form>
                {issuesList ? (
                    <div>
                        {this.renderIssuesList(issues)}
                        <div className="completed">
                            <div>
                                <img src={require('../images/bird.png')} alt=""/>
                                Завершено
                            </div>
                            <div>
                                <span>{this.getCountCompleted()}</span>
                            </div>
                        </div>
                    </div>
                ) : (
                    <h4 className="margin">Задачи отсутствуют</h4>
                )}
            </div>
        );
    }
}

//noinspection JSUnresolvedVariable
MyIssues.propTypes = {
    issuesList: PropTypes.array,
    fullWidthClassName: PropTypes.string,
    setWindows: PropTypes.func,
    checkSession: PropTypes.func
};

export default MyIssues;