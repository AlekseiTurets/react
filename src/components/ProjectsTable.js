import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Moment from 'moment';
import {ISSUE_OPTIONS_LIST, DATE_FORMAT_MONTH_SHORT_DAY_YEAR} from '../constants/Constants';
import {getUsersListFullNames, getUsersIssue} from '../utils/Users';
import '../css/index.css';

class ProjectsTable extends Component {
    componentWillMount() {
        this.props.checkSession();
    }

    createTableList() {
        let {projectIssuesList} = this.props;
        let tableList = null;

        if (projectIssuesList) {
            tableList = projectIssuesList.map((el, index) => {
                return (
                    <tr key={index}>
                        <td>{index}</td>
                        <td>{el.name}</td>
                        <td>{this.getDate(el.dateStart)}</td>
                        <td>{this.getDate(el.dateEnd)}</td>
                        <td>{`${this.getDiffDays(el.dateStart, el.dateEnd)}д.`}</td>
                        <td>{this.getStatus(el.status)}</td>
                        <td>{getUsersListFullNames(getUsersIssue(el), 'issue').substr(2)}</td>
                    </tr>
                )
            });
        }
        return tableList;
    }

    getDiffDays(dateStart, dateEnd) {
        let momentStart = Moment(dateStart);
        let momentEnd = Moment(dateEnd);
        let days = 0;

        if (momentStart && momentEnd) {
            days = momentEnd.diff(momentStart, 'days');
            if (days === 0) {
                days = 1;
            }
        }
        return days;
    }

    getStatus(status) {
        let statusOption = Object.keys(ISSUE_OPTIONS_LIST).find((key) => {
            return ISSUE_OPTIONS_LIST[key].value === status;
        });
        return statusOption.label;
    }

    getDate(date) {
        return Moment(date).format(DATE_FORMAT_MONTH_SHORT_DAY_YEAR);
    }

    render() {
        let {project} = this.props;

        return(
            <div className="ProjectsTable">
                <table>
                    <tbody>
                        <tr>
                            <th></th>
                            <th>Заголовок</th>
                            <th>Начало</th>
                            <th>Срок выполнения</th>
                            <th>Длительность</th>
                            <th>Статус</th>
                            <th>Исполнители</th>
                        </tr>
                        <tr>
                            <td></td>
                            <td>{project.name}</td>
                            <td>{this.getDate(project.dateStart)}</td>
                            <td>{this.getDate(project.dateEnd)}</td>
                            <td>{`${this.getDiffDays(project.dateStart, project.dateEnd)}д.`}</td>
                            <td></td>
                            <td></td>
                        </tr>
                        {this.createTableList()}
                    </tbody>
                </table>
            </div>
        )
    }
}

//noinspection JSUnresolvedVariable,JSUnresolvedFunction
ProjectsTable.propTypes = {
    ProjectsTable: PropTypes.shape({
        projectIssuesList: PropTypes.object,
        project: PropTypes.object,
        checkSession: PropTypes.func,
    })
};

export default ProjectsTable;