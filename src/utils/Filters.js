import {loadData} from '../utils/Storage';

export function filterByParam(data, filteredParam, property, withParam = true) {
    return data.filter(function(item) {
        if (withParam) {
            return filteredParam === item[property];
        } else {
            return filteredParam !== item[property];
        }
    });
}

export function filterByList(data, list, propertyForCompare, resultInList = true) {
    return data.filter(function(item) {
        if (resultInList) {
            return list.indexOf(item[propertyForCompare]) !== -1;
        } else {
            return list.indexOf(item[propertyForCompare]) === -1;
        }
    });
}

export function issuesFilter(projectIssuesList, filterIssueStatus, filterUserId) {
    let issuesFilter = [];
    let status = parseInt(filterIssueStatus, 10);

    if (status !== 0) {
        issuesFilter = projectIssuesList.filter((issue) => {
            // eslint-disable-next-line
            return parseInt(issue.status, 10) === status;
        });
    } else {
        issuesFilter = projectIssuesList;
    }

    if (filterUserId !== 0) {
        const usersIssues = loadData('usersIssues');

        issuesFilter = issuesFilter.filter((issue) => {
            let usersIssuesFiltered = usersIssues.filter((userIssue) => {
                return userIssue.userId === filterUserId && userIssue.issueId === issue.id;
            });
            return usersIssuesFiltered && usersIssuesFiltered.length > 0;
        });
    }
    return issuesFilter;
}