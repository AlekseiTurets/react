import React from "react";
import {loadData} from '../utils/Storage';
import {getFullName} from '../utils/Users';
import {
    MEDIA_WIDTH,
    MENU_LIST,
    MENU_TABLE,
    MENU_TIMELINE
} from '../constants/Constants';

export function getHeight(projectMenu = MENU_LIST) {
    let innerHeight = window.innerHeight;
    let innerWidth = window.innerWidth;

    if (innerWidth > MEDIA_WIDTH || projectMenu === MENU_TABLE || projectMenu === MENU_TIMELINE) {
        return innerHeight - 80;
    } else {
        return parseInt(innerHeight * 0.45, 10);
    }
}

export function getSelect(handleOnChange, isHidden, className) {
    let users = loadData('users');

    users = users.map(function(user, index) {
        return (
            <option key={index} value={user.id}>{getFullName(user)}</option>
        );
    });
    users.unshift(<option key={-1} value=""/>);
    return <select className={className} onChange={handleOnChange} hidden={isHidden} value="">{users}</select>;
}

export function hideUserDeleteButtons() {
    const inputList = document.getElementsByClassName('firstLettersCheckbox');

    if (inputList.length > 0) {
        for (let e of inputList) {
            e.checked = false;
        }
    }
}

export function getIconArrow(show) {
    const iconArrowName = show ? 'arrowOpen' : 'arrowClose';
    return <img src={require(`../images/${iconArrowName}.png`)} alt=""/>;
}

export function focus(e) {
    setTimeout(() => {
        e.focus();
    }, 100);
}

export function getWord(val) {
    let remainder = val % 100;
    let word;

    switch (remainder > 20 ? remainder % 10 : remainder) {
        case 1: {
            word = 'подзадача';
            break;
        }
        case 2:
        case 3:
        case 4: {
            word = 'подзадачи';
            break
        }
        default: word = 'подзадач';
    }
    return word;
}