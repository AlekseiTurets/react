import Moment from 'moment';
import {ISSUE_ACTIVE} from '../constants/Constants';
import {loadData} from '../utils/Storage';
import {loadCurrentUser} from '../utils/Users';
import {filterByList} from '../utils/Filters';

export function getProjectIssuesList(project) {
    let issues = loadData('issues');
    let projectsIssuesList = [];

    if (issues) {
        issues.map(function(item) {
            // noinspection JSUnresolvedVariable
            if (item.projectId === project.id) {
                item['projectName'] = project.name;
                projectsIssuesList.push(item);
            }
            return false;
        });
    }
    return projectsIssuesList;
}

export function createIssue(newIssueId, projectId, newIssueName, newDate) {
    return {
        id: newIssueId,
        projectId: projectId,
        name: newIssueName,
        dateStart: newDate,
        dateEnd: newDate,
        status: ISSUE_ACTIVE,
        description: '',
        author: {
            user: loadCurrentUser(),
            date: Moment()
        }
    };
}

export function getIssuesList(issues = loadData('issues'), usersIssues = loadData('usersIssues')) {
    let issuesList;
    let currentUser = loadCurrentUser();
    let projects = loadData('projects');

    if (currentUser
        && issues
        && issues.length > 0
        && usersIssues
        && usersIssues.length > 0
    ) {
        let issuesId = getListId(usersIssues, 'userId', 'issueId', currentUser.id);

        issuesList = filterByList(issues, issuesId, 'id');
        issuesList = issuesList.map(function(issue) {
            let projectName = '';
            projects.map(function(project) {
                //noinspection JSUnresolvedVariable
                if (project.id === issue.projectId) {
                    projectName = project.name;
                }
                return false;
            });
            issue['projectName'] = projectName;
            return issue;
        });
    }
    return issuesList;
}

function getListId(data, propertyForCompare, propertyForList, compareId) {
    let listId = [];
    data.map(function(item) {
        if (item[propertyForCompare] === compareId) {
            listId.push(item[propertyForList]);
        }
        return false;
    });
    return listId;
}