import React from 'react';
import Moment from 'moment';
import {saveData, loadData} from '../utils/Storage';
import {SESSION_TIME} from '../constants/Constants';

export function getUsers(objectKey, objectId, relationTable, select, listClassName, onClickAddUser, params = {}) {
    let usersList;
    let {label = true, callback} = params;
    let addIco = <div className="usersAddIco"><img onClick={onClickAddUser} className="addImg" src={require('../images/add.png')} alt=""/></div>;
    let usersListId = getUsersListId(objectKey, objectId, relationTable);

    if (usersListId.length > 0) {
        usersList = getListUsersFirstLetters(usersListId, callback);
        usersList = label ? <span>Участники <span className="delimiter">|</span> {usersList}</span> : usersList;
    } else {
        usersList = <span className="addUserText">Добавить исполнителя</span>;
    }
    return (
        <div className={listClassName}>
            {usersList}
            {addIco}
            {select}
        </div>
    );
}

export function getUsersListId(objectKey, objectId, relationTable) {
    let relationTableData = loadData(relationTable);
    let usersListId = [];

    if (relationTableData && relationTableData.length > 0) {
        relationTableData.map(function(item) {
            if (item[objectKey] === objectId) {
                usersListId.push(item.userId);
            }
            return false;
        });
    }
    return usersListId;
}

export function getListUsersFirstLetters(usersListId, callback = null) {
    let usersList;
    let users = loadData('users');
    let usersFiltered = users.filter(function(user) {
        return usersListId.indexOf(user.id) !== -1;
    });
    usersList = usersFiltered.map(function (user, index) {
        return getUserFirstLetters(user, index, callback);
    });

    return usersList;
}

export function getUserFirstLetters(user, key = '', callback = null) {
    return (
        <span className="userFirstLetters" key={key}>
            <span>{user.firstName.charAt(0)}</span>
            <span>{user.lastName.charAt(0)}</span>
            {callback ? (
                <span className="deleteUser">
                    <input type="checkbox" className="firstLettersCheckbox"/>
                    <span className="cursorPointer" onClick={callback} data-user-id={user.id}>Удалить пользователя</span>
                </span>
            ) : null}
        </span>
    );
}

export function loadCurrentUser() {
    let currentUser = localStorage.getItem('currentUser');
    return currentUser ? JSON.parse(currentUser) : null;
}

export function saveCurrentUser(currentUser = null) {
    if (currentUser) {
        currentUser.lastActivity = Moment().unix();
        saveData('currentUser', currentUser);
    } else {
        localStorage.setItem('currentUser', '');
    }
}

export function sessionIsEnded(lastActivity) {
    let diff = Moment().unix() - lastActivity;
    return diff > SESSION_TIME;
}

export function getFullName(user) {
    return `${user.firstName} ${user.lastName}`;
}

export function getUsersListFullNames(usersRel, row) {
    let usersList = '';

    if (usersRel && usersRel.length > 0) {
        usersRel.forEach((el) => {
            usersList = usersList + getFullName(el) + ', ';
        });
    }

    if (usersList !== '') {
        const formattedUsers = usersList.substring(0, usersList.length - 2);
        usersList = row === 'project' ? ' (' + formattedUsers + ')' : ' / ' + formattedUsers;
    }

    return usersList;
}

export function getUsersProject(project) {
    return getUsersList(project, 'usersProjects', 'projectId');
}

export function getUsersIssue(issue) {
    return getUsersList(issue, 'usersIssues', 'issueId');
}

function getUsersList(object, usersRelTable, property) {
    let usersRel = loadData(usersRelTable);
    let usersIdList = [];
    let usersFiltered = [];

    if (usersRel && usersRel.length > 0) {
        usersRel.forEach((el) => {
            if (el[property] === object.id) {
                usersIdList.push(el.userId);
            }
        });
    }

    if (usersIdList.length > 0) {
        let users = loadData('users');
        usersFiltered = users.filter((user) => {
            return usersIdList.indexOf(user.id) !== -1;
        });
    }
    return usersFiltered;
}