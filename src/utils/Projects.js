import {loadData} from '../utils/Storage';

export function getProjectNameById(projectId) {
    const projects = loadData('projects');
    let projectName = '';

    if (projects) {
        let project = projects.find((e) => {
            return e.id === projectId;
        });

        if (project) {
            projectName = project.name;
        }
    }
    return projectName;
}

export function getFirstProjectId() {
    const projects = loadData('projects');
    let firstProjectId = null;

    if (projects && projects.length > 0) {
        firstProjectId = projects[Object.keys(projects)[0]].id;
    }
    return firstProjectId;
}