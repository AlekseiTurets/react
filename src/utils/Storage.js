export function getNewtId(storageKey) {
    let lastInsertId = localStorage.getItem(storageKey);
    return lastInsertId ? parseInt(lastInsertId, 10) + 1 : 1;
}

export function saveNewData(storageDataKey, storageIdKey, oldData, newData, newId) {
    let _newData = oldData ? oldData.concat(newData) : [newData];
    saveData(storageDataKey, _newData);
    saveData(storageIdKey, newId);
    return _newData;
}

export function loadData(key) {
    return JSON.parse(localStorage.getItem(key));
}

export function saveData(key, data) {
    localStorage.setItem(key, JSON.stringify(data));
}