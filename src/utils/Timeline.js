import Moment from 'moment';

export function getTimelineDateRange(projectIssuesList, project) {
    let minDate = null;
    let maxDate = null;

    if (projectIssuesList && projectIssuesList.length > 0) {
        minDate = Moment();
        maxDate = Moment();
        projectIssuesList.forEach((el) => {
            let momentMinDate = Moment(el.dateStart);
            let momentMaxDate = Moment(el.dateEnd);

            if (momentMinDate.isBefore(minDate, 'day')) {
                minDate = momentMinDate
            }

            if (momentMaxDate.isAfter(maxDate, 'day')) {
                maxDate = momentMaxDate
            }
        });
    }

    if (minDate && maxDate) {
        let momentMinDate = Moment(project.dateStart);
        let momentMaxDate = Moment(project.dateEnd);

        if (momentMinDate.isBefore(minDate, 'day')) {
            minDate = momentMinDate
        }

        if (momentMaxDate.isAfter(maxDate, 'day')) {
            maxDate = momentMaxDate
        }
    }
    return {
        minDate: minDate ? minDate.startOf('week') : null,
        maxDate: maxDate ? maxDate.endOf('week') : null
    };
}