export const ISSUE_ALL = 0;
export const ISSUE_ACTIVE = 1;
export const ISSUE_COMPLETED = 2;
export const ISSUE_POSTPONED = 3;
export const ISSUE_CANCELED = 4;

export const PROJECT_ACTIVE = 1;
export const PROJECT_SUSPENDED = 2;
export const PROJECT_CANCELED = 3;

export const ISSUE_OPTIONS_LIST = {
    [ISSUE_ACTIVE]: {value: ISSUE_ACTIVE, label: 'Активна', style: {backgroundColor: '#3894EF'}},
    [ISSUE_COMPLETED]: {value: ISSUE_COMPLETED, label: 'Завершена', style: {backgroundColor: '#8DC257'}},
    [ISSUE_POSTPONED]: {value: ISSUE_POSTPONED, label: 'Отложена', style: {backgroundColor: '#673CB2'}},
    [ISSUE_CANCELED]: {value: ISSUE_CANCELED, label: 'Отменена', style: {backgroundColor: '#9D9D9D'}}
};
export const PROJECT_OPTIONS_LIST = {
    [PROJECT_ACTIVE]: {value: PROJECT_ACTIVE, label: 'Выполнено'},
    [PROJECT_SUSPENDED]: {value: PROJECT_SUSPENDED, label: 'Приостановлено'},
    [PROJECT_CANCELED]: {value: PROJECT_CANCELED, label: 'Завершено'}
};
export const OPTIONS_LIST = {
    [ISSUE_ALL]: {
        value: ISSUE_ALL,
        label: 'Любые',
        style: {backgroundColor: '#cccccc'}
    },
    [ISSUE_ACTIVE]: {
        value: ISSUE_ACTIVE,
        label: 'Активные',
        style: {backgroundColor: '#3894EF'}
    },
    [ISSUE_COMPLETED]: {
        value: ISSUE_COMPLETED,
        label: 'Завершеннные',
        style: {backgroundColor: '#8DC257'}
    },
    [ISSUE_POSTPONED]: {
        value: ISSUE_POSTPONED,
        label: 'Отложенные',
        style: {backgroundColor: '#673CB2'}
    },
    [ISSUE_CANCELED]: {
        value: ISSUE_CANCELED,
        label: 'Отмененные',
        style: {backgroundColor: '#9D9D9D'}
    }
};
export const SECOND_DAY = 86400;
export const DATE_FORMAT = 'Y-MM-DD';
export const DATE_FORMAT_MONTH_DAY = 'MMMM DD';
export const DATE_FORMAT_MONTH_SHORT_DAY = 'MMM DD';
export const DATE_FORMAT_MONTH_SHORT_DAY_YEAR = 'MMM DD, Y';
export const DATE_FORMAT_WEEK_SHORT = 'ddd';
export const DATE_FORMAT_H_MM = 'h:mm';
export const SESSION_TIME = 1200;
export const MEDIA_WIDTH = 1340;
export const MENU_LIST = 'list';
export const MENU_TABLE = 'table';
export const MENU_TIMELINE = 'timeline';

export const WINDOW_ISSUE_DETAIL = 'WINDOW_ISSUE_DETAIL';
export const WINDOW_PROJECT = 'WINDOW_PROJECT';